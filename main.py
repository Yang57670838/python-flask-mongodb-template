from dotenv import dotenv_values
from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from pymongo import MongoClient
from bson.objectid import ObjectId
import numpy as np

# load env variables
env_config = dotenv_values(".env")

# connect to mongodb
client = MongoClient(env_config["DB_URL"])
db = client[env_config["DB_NAME"]] # db name
companies_collection = db[env_config["DB_COLLECTION_NAME_FOR_COMPANY"]] # table collection

app = Flask(__name__)
api = Api(app)
        
# TODO: switch to use firestore for company
def is_company_exists(email, name):
    '''
    check if company is existing from database.
        Parameters:
            email(str): email address which belong to a company
            name(str): name of company

        Returns:
            boolean: True or False
    '''
    result = companies_collection.count_documents({"email": email, "name": name})
    if result == 0:
        return False
    else: 
        return True
    
class Company(Resource):
    def post(self):
        payload = request.get_json()
        if "email" not in payload or "name" not in payload:
            return "Missing informations", 400
        email = payload['email']
        name = payload['name']
        try:
            result = {
                'email': email,
                'name': name
            }
            companies_collection.insert_one(result)
        except:
            return "Invalid informations", 400   
        else:
            return "Success", 200   
    
api.add_resource(Company, '/company') 

@app.route('/')
def hello_world():
    return "Hello World!"

@app.route('/analyse', methods=['POST'])
def analyse():
    '''
    webhook for receiving push msg from gcp pub/sub, about we want to do some analyse job for whole,
    store the analyse result in db 
    '''
    return "Success", 200   

if __name__ == "__main__":
    # TODO:  remove debug=True in production
    # Required for Cloud Run for host 0.0.0.0
    app.run(host="0.0.0.0", port=env_config["PORT"], debug=True)
