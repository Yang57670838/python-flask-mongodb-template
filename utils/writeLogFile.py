from datetime import datetime

# write logs into txt file
def write_logs_to_file(filepath):
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open(filepath, "a") as file:
        file.write(f"[{timestamp}] New log entry added!\n")


file_path = "../log.txt"
write_logs_to_file(file_path) 
print(f"✅ Data written to {file_path}")