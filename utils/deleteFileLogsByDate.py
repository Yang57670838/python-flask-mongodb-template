from datetime import datetime

# Condition to remove lines if the datetime matched
# log format [2025-02-12 23:34:54] New log entry added!
def should_remove(datetimeString, filterDatetimeBefore):
    formattedDatetime = datetime.strptime(datetimeString, "%Y-%m-%d %H:%M:%S")
    return formattedDatetime < filterDatetimeBefore

def delete_logs__from_file_by_date(filepath, filterBeforeDatetime):
    with open(filepath, "r") as file:
        lines = file.readlines()

    filtered_lines = [line for line in lines if not should_remove(line[1:20], filterBeforeDatetime)] 
    # Write back only the remaining lines
    with open(filepath, "w") as file:
        file.writelines(filtered_lines)
    print(f"Removed matching lines from {filepath}")
            


file_path = "../log.txt"
filterBeforeDatetime = datetime.now()
delete_logs__from_file_by_date(file_path, filterBeforeDatetime) 







