
def read_log_file(filepath):
    with open(filepath, "r") as file:
        for line in file:
            print(line.strip())  # Removes newline characters


file_path = "../log.txt"
read_log_file(file_path) 