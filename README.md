### setup in mac locally
python3 -m ensurepip --upgrade <br />
python3 -m venv venv <br />
source venv/bin/activate <br />
pip3 install -r requirements.txt <br />

### setup in server
python3 -m ensurepip --upgrade <br />
pip3 install -r requirements.txt <br />

### lint test
black . <br />
pylint * <br />

### everytime update packages in local
pip3 freeze > requirements.txt

### run/stop docker compose
docker-compose up -d <br />
docker-compose down <br />


## TODO
1, webhook endpoint for pub/sub service, so it will get request from pub/sub to do some overall analyse job through database <br />

